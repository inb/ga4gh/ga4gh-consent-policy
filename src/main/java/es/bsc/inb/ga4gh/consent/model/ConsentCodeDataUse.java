/**
 * *****************************************************************************
 * Copyright (C) 2021 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.ga4gh.consent.model;

import java.util.List;

/**
 * @author Dmitry Repchevsky
 */

public class ConsentCodeDataUse {
    
    private ConsentCodeDataUseCondition primary_category;
    private List<ConsentCodeDataUseCondition> secondary_categories;
    private String version;
    
    public ConsentCodeDataUse() {}
    
    public ConsentCodeDataUseCondition getPrimaryCategory() {
        return primary_category;
    }
    
    public void setPrimaryCategory(ConsentCodeDataUseCondition primary_category) {
        this.primary_category = primary_category; 
    }
    
    public List<ConsentCodeDataUseCondition> getSecondaryCategories() {
        return secondary_categories;
    }
    
    public void setSecondaryCategories(List<ConsentCodeDataUseCondition> secondary_categories) {
        this.secondary_categories = secondary_categories;
    }
    
    public String getVersion() {
        return version;
    }
    
    public void setVersion(String version) {
        this.version = version;
    }
}
